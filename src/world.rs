pub const WIDTH: u32 = 320;
pub const HEIGHT: u32 = 240;

/// Representation of the application state. In this example, a box will bounce around the screen.
pub struct World {}

impl World {
    /// Create a new `World` instance that can draw a moving box.
    pub fn new() -> Self {
        Self {}
    }

    /// Update the `World` internal state; bounce the box around the screen.
    pub fn update(&mut self) {}

    /// Draw the `World` state to the frame buffer.
    ///
    /// Assumes the default texture format: [`pixels::wgpu::TextureFormat::Rgba8UnormSrgb`]
    pub fn draw(&self, frame: &mut [u8]) {
        for pixel in frame.chunks_exact_mut(4) {
            let rgba = [0, 40, 80, 0xff];
            pixel.copy_from_slice(&rgba);
        }
    }
}
