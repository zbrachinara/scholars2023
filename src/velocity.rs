use rand::thread_rng;

use crate::point_state::rand_vecs;
use glam::Vec2;

pub struct Velocity {
    velocity: Vec2,
    /// The period of a single movement cycle.
    rate: f32,
    /// The amount through the movement cycle we have advanced
    state: f32,
}

impl Default for Velocity {
    fn default() -> Self {
        use rand::Rng;
        let mut rng = thread_rng();

        let rate = rng.gen_range(3f32..10f32);
        let init_state = rng.gen_range(0f32..rate);

        let mut slf = Self {
            velocity: Default::default(),
            rate,
            state: init_state,
        };

        slf.set_velocity();
        slf
    }
}

impl Velocity {
    fn set_velocity(&mut self) {
        let mut rng = thread_rng();
        let v2 = rand_vecs::<1>(&mut rng, -1f32..1f32)[0];
        let vel = Vec2 { x: v2.0, y: v2.1 };

        self.velocity = vel;
    }

    /// given an initial point in space and a change in time, returns the new point in space (NOT
    /// the change in space!)
    pub fn update(&mut self, delta_time: f32, point: Vec2) -> Vec2 {
        self.state += delta_time;
        let del = 0.001;

        if point.x < 0. + del || point.x > 1.-del {
            println!("reflected x");
            self.velocity.x *= -1.;
        }
        if point.y < 0.+del || point.y > 1. -del{
            println!("reflected y");
            self.velocity.y *= -1.;
        }

        if self.state > self.rate {
            self.set_velocity();
            self.state = 0.;
        }

        (point + self.velocity * 0.006 * f32::exp(-self.state * 0.8))
            .clamp(Vec2::splat(0.), Vec2::splat(1.))
    }
}
