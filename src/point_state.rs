use std::{fmt::Display, ops::Range};

use glam::Vec2;
use pixels::wgpu::{self, util::DeviceExt};
use rand::Rng;

use crate::velocity::Velocity;

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Vec2Padded {
    x: f32,
    y: f32,
    _padding: [u8; 8],
}

impl From<Vec2> for Vec2Padded {
    fn from(value: Vec2) -> Self {
        Self::from((value.x, value.y))
    }
}

impl From<&Vec2Padded> for Vec2 {
    fn from(value: &Vec2Padded) -> Self {
        Self::from((value.x, value.y))
    }
}

impl From<(f32, f32)> for Vec2Padded {
    fn from((x, y): (f32, f32)) -> Self {
        Self {
            x,
            y,
            _padding: [0; 8],
        }
    }
}

impl Display for Vec2Padded {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let as_glam_vec: Vec2 = self.into();
        write!(f, "{as_glam_vec}")
    }
}

const NUM_POINTS: usize = 50;

pub struct PointState {
    points: [Vec2Padded; NUM_POINTS],
    velocities: [Velocity; NUM_POINTS],
    time: f32,
}

fn blank_array<const N: usize>() -> [(); N] {
    [(); N]
}

pub fn rand_vecs<const N: usize>(rng: &mut impl Rng, range: Range<f32>) -> [(f32, f32); N] {
    blank_array().map(|_| (rng.gen_range(range.clone()), rng.gen_range(range.clone())))
}

impl Default for PointState {
    fn default() -> Self {
        let mut rng = rand::thread_rng();
        Self {
            points: rand_vecs(&mut rng, 0f32..1f32).map(Vec2Padded::from),
            velocities: blank_array().map(|_| Default::default()),
            time: 0.0,
        }
    }
}

impl PointState {
    pub fn layout(binding: u32) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding,
            visibility: wgpu::ShaderStages::FRAGMENT,
            ty: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        }
    }

    pub fn to_buffer(&self, device: &wgpu::Device) -> wgpu::Buffer {
        device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Voronoi Points Buffer"),
            contents: bytemuck::cast_slice(&self.points),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        })
    }

    pub fn update(&mut self, time: f32) -> &[Vec2Padded] {
        let delta = (time - self.time).abs();
        self.time = time;

        self.points
            .iter_mut()
            .zip(self.velocities.iter_mut())
            .for_each(|(point, vel)| {
                *point = vel.update(delta, (&*point).into()).into();
                print!("{point} ");
            });
        println!();

        &self.points
    }
}
