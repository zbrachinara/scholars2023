// Vertex shader bindings

struct VertexOutput {
    @location(0) tex_coord: vec2<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn vs_main(
    @location(0) position: vec2<f32>,
) -> VertexOutput {
    var out: VertexOutput;
    out.tex_coord = fma(position, vec2<f32>(0.5, -0.5), vec2<f32>(0.5, 0.5));
    out.position = vec4<f32>(position, 0.0, 1.0);
    return out;
}

// Fragment shader bindings

@group(0) @binding(0) var r_tex_color: texture_2d<f32>;
@group(0) @binding(1) var r_tex_sampler: sampler;
struct wrapped_vec2 { 
    @size(16) v: vec2<f32>
}
const length: u32 = 25u;
struct Locals {
    points: array<wrapped_vec2,length>,
}
@group(0) @binding(2) var<uniform> r_locals: Locals;
@group(0) @binding(3) var<uniform> time: f32;

fn hsv_to_rgb(c: vec3f) -> vec3f {
    let K = vec4f(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    let p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, vec3f(0.), vec3f(1.)), c.y);
}

fn lerpc(c1: vec3f, c2: vec3f, t: f32) -> vec3f {
    return (c2 - c1) * t + c1;
}

fn overlay_colors(over: vec4f, under: vec4f) -> vec4f {
    let alpha = (1.-over.w) * under.w + over.w;
    return vec4f(
        ((1.-over.x)*under.w*under.x+over.x*over.w)/alpha,
        ((1.-over.y)*under.w*under.y+over.y*over.w)/alpha,
        ((1.-over.z)*under.w*under.z+over.z*over.w)/alpha,
        alpha,
    );
}

fn two_closest(p: vec2f) -> vec2u {
    var minimum = 100.0;
    var minimum_ix = 0u;
    var minimum2 = 100.0;
    var minimum2_ix = 0u;
    for (var i = 0u; i < length; i++) {
        var distance = distance(r_locals.points[i].v, p);
        if (minimum > distance) {
            // move old minimum into second minimum
            minimum2_ix = minimum_ix;
            minimum2 = minimum;
            // fill new minimum
            minimum = distance;
            minimum_ix = i;
        } 
        else if (minimum2 > distance) {
            minimum2 = distance;
            minimum2_ix = i;
        }
    }

    return vec2u(minimum_ix, minimum2_ix);
}

@fragment
fn fs_main(@location(0) tex_coord: vec2<f32>) -> @location(0) vec4<f32> {
    let sampled_color = textureSample(r_tex_color, r_tex_sampler, tex_coord);

    let closest = two_closest(tex_coord);
    let first_closest = closest.x;
    let second_closest = closest.y;

    let dst1 = distance(r_locals.points[first_closest].v, tex_coord);
    let dst2 = distance(r_locals.points[second_closest].v, tex_coord);
    
    let duration = 5.;
    let t = time % duration;
    let min = .01;
    var limit_point = 0.;
    if (t < .8*duration) {
        limit_point = min + .5 * t/.8/duration;
    } else {
        limit_point = min + .5 * (duration-t)/.2/duration;
    }
    let depth = (limit_point - abs(dst1-dst2))/limit_point;

    if (depth > 0.) {
        let color = hsv_to_rgb(vec3f(f32(first_closest)/f32(length), 1.-depth,1.) );
        // return overlay_colors(vec4f(color,depth), sampled_color);
        return vec4(lerpc(sampled_color.rgb, color, depth), 1.);
    } else {
        return sampled_color;
    }
}
